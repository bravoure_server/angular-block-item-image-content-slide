# Bravoure - Block Item Image Content Slide

## Directive: Code to be added:

        <block-item-image-content-slide></block-item-image-content-slide>

## Use

It can be added in the cms under the option of "blocks", and inserting the block-list-slider,
and inserting the content of each slide.


### Instalation

in the bower.json file of the base of the application,(src/app/bower.json)

add

    {
      ...
      "dependencies": {
        ...
        "angular-block-item-image-content-slide": "1.0"
      }
    }

and the run in the terminal 
    
    // in the correct location (src/app)
    
    bower install
