(function () {

    'use strict';

    function blockItemImageContentSlide (PATH_CONFIG, $timeout, $analytics, $rootScope) {

        return {
            restrict: "E",
            transclude: false,
            replace: true,

            link: function (scope, element, attrs) {

                element.on('click', 'a', function () {

                    $analytics.eventTrack('Click', {
                        category: 'Click',
                        action: 'List - Click',
                        label: $(this).attr('title') + ': ' + $(this).attr('href')
                    });

                });

            },

            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

                $rootScope.$emit('Loading', $rootScope.loadedElements);

                $scope.option = {
                    // Optional parameters
                    loop: false,
                    pagination: '.swiper-pagination-' + $scope.module.id,
                    paginationClickable: true,
                    autoplay: 5000,
                    speed: 1000
                };

                $scope.$watch('module.items', function () {
                    $timeout(function () {
                        if ($scope.module.items.length > 1) {
                            $scope.mySwiper = new Swiper('.swiper-' + $scope.module.id, $scope.option);
                        }
                    }, 10);
                });

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-item-image-content-slide/block-item-image-content-slide.html'
        }
    }

    blockItemImageContentSlide.$inject = ['PATH_CONFIG', '$timeout', '$analytics', '$rootScope'];

    angular
        .module('bravoureAngularApp')
        .directive('blockItemImageContentSlide', blockItemImageContentSlide);

})();
